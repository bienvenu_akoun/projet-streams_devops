<?php
session_start();  // démarrage d'une session

// on vérifie que les variables de session identifiant l'utilisateur existent
if ($_SESSION["authenOK"] == true) 
{

?>

<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title> STREAMING </title>
  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css'>
  <link rel="stylesheet" href="./../CSS/style.css">  <!-- Liaison avec le fichier CSS -->
</head>

<body>

  <!-- Section de navigation -->
<header>
  <div class="navbar">

  <li class="list-item"><a href="./accueilStreamer.php">
    <i class="fa-solid fa-house"></i>
   </a>
  </li>

<li class="list-item">
    <select id="esportSelect" name="Choix" onChange="location.href=''+ '//localhost/projet-streams_devops/PHP/accueilStreamer.php?streamer=' +this.options[this.selectedIndex].value;"> 
        <option value="#" align="center">ESPORTS</option>

    </select>    </i>                                                                                                     
</li>

<li class="list-item">
    <select id="gameSelect" name="Choix" onChange="location.href=''+ '//localhost/projet-streams_devops/PHP/accueilStreamer.php?streamer=' +this.options[this.selectedIndex].value;"> 
        <option value="#" align="center">JEUX </option>
    </select>    </i>                                                                                                     
</li>

<li class="list-item">
    <select id="musicSelect" name="Choix" onChange="location.href=''+ '//localhost/projet-streams_devops/PHP/accueilStreamer.php?streamer=' +this.options[this.selectedIndex].value;"> 
        <option value="#" align="center">MUSIQUE</option>
    </select>    </i>                                                                                                     
</li>

<li class="list-item">
    <select name="Choix" onChange="location.href=''+this.options[this.selectedIndex].value+'.php';"> 
        <option value="#" align="center">GERER SON COMPTE</option>
        <option value="./changerMotDePasse">Modifier son mot de passe </option>
        <option value="./changerEmail">Modifier son email</option>
        <option value="./changerSonNomUtilisateur">Modifier son nom d'utilisateur</option>
        <option value="./supprimerCompte">Supprimer son compte</option>
        <option value="./logout">Déconnexion</option>
    </select>    </i>                                                                                                     
</li>
  
  </div>

</header>  </br>


 <!-- Vidéo et image en fond --> 

<div id="fondpage"> 
 
 <!-- Bloc commentaire --> 
<div id="recommandation"> 
  <h2>STREAMERS EN LIGNE</h2>
  <ul id="listRecommendation" class="list-unstyled">
    
  </ul>
</div> </br>

	
	 <!-- Bloc streaming --> 
<div id="video">
		<!-- Ajout de la vidéo -->
    <div id="twitch-embed"></div>

    <!-- Load the Twitch embed JavaScript file -->
    <script src="https://embed.twitch.tv/embed/v1.js"></script>

    <!-- Create a Twitch.Embed object that will render within the "twitch-embed" element -->
    <script type="text/javascript">
      new Twitch.Embed("twitch-embed", {
        width: 1000,
        height: 450,
        channel: "<?php echo $_GET["streamer"]; ?>"
      });
    </script>

</div>		
</div> 



<!-- Pied de la page -->
<footer> 
  <footer class="copyright">   
      <h4>Pour nous contacter<a href="./../HTML/contact.html" target="_blank"> cliquez ici </a></h4>
</footer>

<script src="../JS/script.js"></script>
<script>getRecommendationList()</script>
</body>
</html>


<?php 
}


else {

  // Redirection à la page d'accueil
 require('index.html');
}

?>