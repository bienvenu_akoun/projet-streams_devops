import { Component, Input, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-list-stream-element',
  templateUrl: './list-stream-element.component.html',
  styleUrl: './list-stream-element.component.css'
})
export class ListStreamElementComponent {
  @Input() streamerName:string
  @Input() nbViewer!:number
  @Input() gameName!:string
}
