import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { faLaptop, faMobile, faHouse, faAddressCard, faUser } from '@fortawesome/free-solid-svg-icons';

declare let Twitch: any;
@Component({
  selector: 'app-acceuil',
  templateUrl: './acceuil.component.html',
  styleUrl: './acceuil.component.css'
})
export class AcceuilComponent implements OnInit{
  faLaptop = faLaptop
  faMobile = faMobile
  faHouse = faHouse
  faAddressCard = faAddressCard
  faUser = faUser
  streams: any

  private apiUrl = 'https://api.twitch.tv/helix/streams?language=fr&first=15';
  private clientId = 'pkx2favy4yo97cuimjqle2j8df3nif';
  private accessToken = 'ap5ygdl7rcenhag0kwu3nbc2tn4965';

  public constructor (private http: HttpClient) {}

  ngOnInit() {
    //liste des streamers
    let headers = new HttpHeaders()
      .set("Client-ID", this.clientId)
      .set("Authorization", "Bearer " + this.accessToken);

    this.http.get(this.apiUrl, {headers: headers}).subscribe((response) => {
     this.streams = (<any>response).data 
    });

    //intégration player + chat
    var options = {
      width: 1000,
      height: 500,
      channel: "tonton",
    };
    
    var player = new Twitch.Embed("twitch-embed", options);
    player.setVolume(0);
  }
}
