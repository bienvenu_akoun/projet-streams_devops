<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="./../CSS/styleLogin.css" />
</head>
<body>

<?php

// Démarrage d'une session
session_start();

if (isset($_REQUEST['email'])&& isset($_REQUEST['motDePasse']))  {

$login = $_REQUEST['email'] ;
$mdp = $_REQUEST['motDePasse'] ;
$_SESSION["authenOK"] = false ;
   
// Connexion à la bd
require('connexionbd.php');

// Requête pour récupérer l'utilisateur depuis la BD
$query = $access->prepare("SELECT * FROM utilisateurs WHERE email = ?");

$query->execute([$_REQUEST['email']]); 
$client = $query->fetch();


// Vérification des identifiants dans la bd
if ($client && password_verify($_REQUEST['motDePasse'], $client['mdp']))
{
	echo "<div class='sucess'>
             <h3>Identifiant  valide !!! </h3>
            </div>";
	         
// Redirection vers la page correspondante en fonction du type d'utilisateur
   
		// L'utilisateur existe dans la table on ajoute ses infos en tant que variables de session
        $_SESSION['email'] = $login;
        $_SESSION['motDePasse'] = $mdp;
		    $_SESSION["authenOK"] = true ;    // cette variable indique que l'authentification a réussi
        
      // Redirection vers la page d'accueil de l'abonné
	  header('Location: ./../PHP/accueilStreamer.php');
  
} 
else { 
	// Sinon message d'erreur et l'utilisateur doit reprendre
    echo "<div class='sucess'>
             <h3>Identifiant non valide !!! </h3>
             <p>Veuillez recommencer</a></p>
       </div>";

}

}

?>

<! FORMULAIRE D'INSCRIPTION >

<form class="box" action="" method="post" name="login">

<h1 class="box-title">Connexion</h1>
 <! Champ pour le mail>
     <h3 align="center"> Email * : </h3>
     <input type="email" class="box-input" name="email" 
     placeholder="" required />
  
     <! Champ pour le mot de passe>
     <h3 align="center" > Mot de passe * :</h3> 
    <input type="password" class="box-input" name="motDePasse" 
  placeholder="" required />
  
  <! Bouton pour valider les identifiants>
<input type="submit" value="Connexion " name="submit" class="box-button">
<p class="box-register">Pour créer un compte,  
  <a href="./inscriptionSurSite.php">cliquez ici</a>
</p>

<?php if (! empty($message)) { ?>
    <p class="errorMessage"><?php echo $message; ?></p>
<?php } ?>
</form>
</body>
</html>