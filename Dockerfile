# Utilisez une image de base contenant PHP et Apache
FROM php:apache

# Copiez les fichiers PHP, CSS et JavaScript dans le répertoire de travail de l'image
COPY . /var/www/html/

# Exposez le port 80 pour permettre l'accès à votre application web
EXPOSE 80

# Installez les extensions PHP nécessaires pour se connecter à MySQL
RUN apt-get update && \
    apt-get install -y libmariadb-dev-compat && \
    docker-php-ext-install mysqli pdo pdo_mysql
