import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientModule } from '@angular/common/http';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ListStreamElementComponent } from './list-stream-element/list-stream-element.component';
import { AcceuilComponent } from './acceuil/acceuil.component';
import { StreamComponent } from './stream/stream.component';

import { LocationStrategy ,PathLocationStrategy } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    ListStreamElementComponent,
    AcceuilComponent,
    StreamComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FontAwesomeModule
  ],
  providers: [{provide: LocationStrategy ,useClass: PathLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule { }
