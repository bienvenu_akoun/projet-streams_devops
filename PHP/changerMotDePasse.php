<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="./../CSS/styleLogin.css" />
</head>
<body>

<?php


if(isset($_POST['submit'])){

if (isset($_REQUEST['email'])&& isset($_REQUEST['motDePasse'])
	&& isset($_REQUEST['NewMotDePasse'])&& isset($_REQUEST['ConfirmNewMotDePasse']))
{
	// Connexion à la bd
    require('connexionbd.php');


  // récupérer les données saisies par l'utilisateur
  
  $email = $_REQUEST['email'];
  $AncienMdp = password_hash($_REQUEST['motDePasse'], PASSWORD_DEFAULT);
  $NewMdp = password_hash($_REQUEST['NewMotDePasse'], PASSWORD_DEFAULT);
  $ConfirmNewMdp = password_hash($_REQUEST['ConfirmNewMotDePasse'], PASSWORD_DEFAULT);
  
// On vérifie le mail dans la bd  	
$stmt = $access->prepare("SELECT * FROM utilisateurs WHERE email=?");
$stmt->execute([$email]); 
$client = $stmt->fetch();

if ($client) {  // Si le mail existe déjà alors impossible

	// On vérifie que l'ancien mot de passe est correct
	if ($client && password_verify($_REQUEST['motDePasse'], $client['mdp']))
	{
		// On vérifie que le nouveau mot de passe a bien été confirmé 
	    if($_REQUEST['NewMotDePasse'] == $_REQUEST['ConfirmNewMotDePasse']) 
		{
			// On vérifie que le nouveau mot de passe est différent de l'ancien
			if($_REQUEST['NewMotDePasse'] != $_REQUEST['motDePasse'])
			{
				// Requete de mise à jour 			
				$ordreSQL = "UPDATE utilisateurs SET mdp = '$NewMdp' Where email LIKE '$email' " ;
				// Exécution de la requête avec la méthode exec et récupération du nombre de tuples modifié
				$nb = $access -> exec($ordreSQL) ;
				// Vérification
					if($nb != 0) 
					{
						echo "<div class='sucess'>
							<h3>Mot de passe modifié avec succès </h3>
							</div>";			}
					else 
					{
						echo "<div class='sucess'>
							<h3>Echec de modification du mot de passe </h3>
							</div>";			
					}																
			}
			else 
			{
	 		echo "<div class='sucess'>
             		<h3>Erreur : Le nouveau mot de passe est identique à l'ancien !</h3>
            		</div>";			
			}
		}  
		else 
		{
	 		echo "<div class='sucess'>
             		<h3>Erreur dans la confirmation du nouveau mot de passe</h3>
            		</div>";		
		}
	}
	else 
	{
	 	echo "<div class='sucess'>
             <h3>Ancien mot de passe incorrect </h3>
            </div>";		
	}
}
	
else { 
 	echo "<div class='sucess'>
             <h3>Adresse mail incorrecte </h3>
            </div>";
}

}
  	  	
} // Fin du if(isset($_POST['submit']))
 
else{
?>
	<! Formulaire de modification de mail >

     <form class="box" action="" method="post">
      <h1 class="box-title">MODIFIER MOT DE PASSE</h1>
   

    <! Champ pour le mail>
     <h3 align="center"> Email * : 
     <input type="email" class="box-input" name="email" 
     placeholder="" required /> </h3>
     
    <! Champ pour le mot de passe actuel >
     <h3 align="center" > Mot de passe actuel * :</h3> 
    <input type="password" class="box-input" name="motDePasse" 
  placeholder="" required />

    <! Champ pour le nouveau mot de passe  >
    <h3 align="center" > Nouveau mot de passe * :</h3> 
    <input type="password" class="box-input" name="NewMotDePasse" 
  placeholder="" required />

    <! Champ pour confirmer le nouveau mot de passe  >
    <h3 align="center" > Confirmer le nouveau mot de passe * :</h3> 
    <input type="password" class="box-input" name="ConfirmNewMotDePasse" 
  placeholder="" required />

     
	<! Bouton de validation >  
    <input type="submit" name="submit" 
  value="Modifier le mot de passe" class="box-button" />
  
</form>
<?php } ?>
</body>
</html>







