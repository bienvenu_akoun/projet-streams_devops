/*
const root = document.documentElement;
const eye = document.getElementById('eyeball');
const beam = document.getElementById('beam');
const passwordInput = document.getElementById('password');

root.addEventListener('mousemove', (e) => {
  let rect = beam.getBoundingClientRect();
  let mouseX = rect.right + (rect.width / 2); 
  let mouseY = rect.top + (rect.height / 2);
  let rad = Math.atan2(mouseX - e.pageX, mouseY - e.pageY);
  let degrees = (rad * (20 / Math.PI) * -1) - 350;

  root.style.setProperty('--beamDegrees', `${degrees}deg`);
});

eye.addEventListener('click', e => {
  e.preventDefault();
  document.body.classList.toggle('show-password');
  passwordInput.type = passwordInput.type === 'password' ? 'text' : 'password'
  passwordInput.focus();
});
*/

let nomUser = ''; // Variable pour stocker le nom d'utilisateur

function setUsername() {
  const usernameInput = document.getElementById('username');
  nomUser = usernameInput.value;
}

function sendMessage() {
  const messageInput = document.getElementById('message-input');
  const message = messageInput.value;

  // Vérifie si le nom d'utilisateur a été saisi et validé
  if (nomUser.trim() === '') {
    alert('Veuillez saisir et valider un nom d\'utilisateur avant d\'envoyer un message.');
    return;
  }

  // Vérifie si le message est vide
  if (message.trim() === '') {
    return; // Ne rien faire si le message est vide
  }

  const chatMessages = document.getElementById('chat-messages');
  const messageDiv = document.createElement('div');

  // Utilise une balise <span> pour le nom d'utilisateur et applique le style
  const usernameSpan = document.createElement('span');
  usernameSpan.style.fontWeight = 'bold';
  usernameSpan.textContent = `${nomUser} : `;

  // Ajoute le span du nom d'utilisateur et le message au div du message
  messageDiv.appendChild(usernameSpan);
  messageDiv.innerHTML += `${message}`;

  // Style du texte du message
  messageDiv.style.fontSize = '14px'; // Ajuste la taille du texte

  // Ajoute une ligne sautée entre chaque message
  messageDiv.style.marginBottom = '8px';

  chatMessages.appendChild(messageDiv);

  // Effacer le champ de saisie après l'envoi du message
  messageInput.value = '';
}


//Liste des recommendations
const clientId = 'pkx2favy4yo97cuimjqle2j8df3nif';
const accessToken = 'ap5ygdl7rcenhag0kwu3nbc2tn4965';
const apiUrl = 'https://api.twitch.tv/helix/streams?language=fr&first=12';

function getRecommendationList() {
  const xhr = new XMLHttpRequest();
  var recommendationList = document.getElementById("listRecommendation")
  var esportList = document.getElementById("esportSelect")
  var gameList = document.getElementById("gameSelect")
  var musicList = document.getElementById("musicSelect")

  xhr.open('GET', apiUrl, true);
  xhr.setRequestHeader('Client-ID', clientId);
  xhr.setRequestHeader('Authorization', 'Bearer ' + accessToken);
  xhr.onreadystatechange = function() {
      if (xhr.readyState === 4 && xhr.status === 200) {
          const response = JSON.parse(xhr.responseText);
          response.data.forEach(stream => {
            linkElement =document.createElement("a")
            linkElement.setAttribute("href", "//localhost/projet-streams_devops/PHP/accueilStreamer.php?streamer=" + stream.user_name)
            listElement = document.createElement("li");
            listElement.setAttribute("style", "list-style-type: none;");

            streamerParagraph = document.createElement("p");
            streamerParagraph.appendChild(document.createTextNode(stream.user_name));
            gameName = document.createElement("p");
            //gameName.appendChild(document.createTextNode(stream.game_name));

            listElement.appendChild(streamerParagraph);
            //listElement.appendChild(gameName);
            linkElement.appendChild(listElement)
            recommendationList.appendChild(linkElement);


            //select
            if (!!esportList && !!gameList && !!musicList) {
              esportList.add(selectStream(stream))
              gameList.add(selectStream(stream))
              musicList.add(selectStream(stream))
            }
            
          });
      }
  };
  xhr.send();
}


function selectStream(stream) {
  option = document.createElement("option");
  link = document.createElement("a")
  option.value = stream.user_name;
  option.text = stream.user_name;
  return option;

}

//Grid stream accueil
function streamPlayerList() {
  const xhr = new XMLHttpRequest();
  list = document.getElementById("twitch-embed-list")

  xhr.open('GET', apiUrl, true);
  xhr.setRequestHeader('Client-ID', clientId);
  xhr.setRequestHeader('Authorization', 'Bearer ' + accessToken);
  xhr.onreadystatechange = function() {
      if (xhr.readyState === 4 && xhr.status === 200) {
          const response = JSON.parse(xhr.responseText);
          response.data.forEach(stream => {
            twitchElement = document.createElement("div");
            twitchElement.setAttribute("class", "box");
            new Twitch.Player(twitchElement, {
              width: 300,
              channel: stream.user_name,
              muted: true
            });
            list.appendChild(twitchElement);
          });
      }
  };
  xhr.send();
}

