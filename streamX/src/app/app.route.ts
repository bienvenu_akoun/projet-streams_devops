import { Routes } from '@angular/router';
import { AcceuilComponent } from './acceuil/acceuil.component';
import { StreamComponent } from './stream/stream.component';

export const routes = [
    { path: 'accueil', component: AcceuilComponent },
    { path: 'stream/:streamerName', component: StreamComponent}
];