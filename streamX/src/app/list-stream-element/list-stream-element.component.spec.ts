import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListStreamElementComponent } from './list-stream-element.component';

describe('ListStreamElementComponent', () => {
  let component: ListStreamElementComponent;
  let fixture: ComponentFixture<ListStreamElementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ListStreamElementComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ListStreamElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
