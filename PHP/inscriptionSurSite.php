
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="./../CSS/styleLogin.css" />
</head>
<body>

<?php


if(isset($_POST['submit'])){

// Connexion à la bd
 require('connexionbd.php');


// récupérer les données saisies par l'utilisateur
  
  $nom_utilisateur = $_POST['nom_utilisateur'];
  $email = $_POST['email'];
  $mdp = password_hash($_POST['mdp'], PASSWORD_DEFAULT);
  
  
  
// Vérification dans la bd si le mail existe déjà
    
$stmt = $access->prepare("SELECT * FROM utilisateurs WHERE email=?");
$stmt->execute([$email]); 
$client = $stmt->fetch();

if ($client) {  // Si le mail existe déjà alors impossible

  echo "<div class='sucess'>
             <h3>Adresse mail non disponible</h3>
             <p>Cliquez ici pour <a href='inscriptionSurSite.php'>recommencer</a></p>
       </div>"; 
}

else {  

// Requête mysql pour insérer des données dans la base de données
  
  $sql = "INSERT into utilisateurs(`nom_utilisateur`,`mdp`,`email`) 
  VALUES (:nom_utilisateur,:mdp,:email)";

  $res = $access->prepare($sql);
  $exec = $res->execute(array(":nom_utilisateur"=>$nom_utilisateur,":email"=>$email,":mdp"=>$mdp));
  
// vérifier si la requête d'insertion a réussi
  if($exec){
    echo "<div class='sucess'>
             <h3>Votre compte a bien été créé.</h3>
             <p>Cliquez ici pour vous <a href='login.php'>connecter</a></p>
       </div>"; 
  }
  else{
    echo "<div class='sucess'>
             <h3>Adresse mail non disponible</h3>
             <p>Cliquez ici pour vous <a href='inscriptionSurSite.php'>recommencer</a></p>
       </div>";  }
}
}


else{
?>
  <! Formulaire d'inscription >

<form class="box" action="" method="post">
      <h1 class="box-title">Formulaire d'inscription</h1>
   
    <! Champ pour le nom >
    <h3 align="center"> Nom * :  
    <input type="text" class="box-input" name="nom_utilisateur" 
     placeholder="" required /> </h3>

    <! Champ pour le mail>
     <h3 align="center"> Email * :
     <input type="text" class="box-input" name="email" 
     placeholder="Email" required /> </h3>
       
     <! Champ pour le mot de passe>
     <h3 align="center" > Mot de passe * :
    <input type="password" class="box-input" name="mdp" 
  placeholder="" required /> </h3> 
  
  <! Bouton de validation >
    <input type="submit" name="submit" 
  value="S'inscrire" class="box-button" />
  
    <p class="box-register">Vous avez déjà un compte ? 
  <a href="login.php">cliquez ici pour vous connecter</a></p>
</form>
<?php } ?>
</body>
</html>
