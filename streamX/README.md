# Installer Node

- Installer la dernière version disponible de node : 
https://nodejs.org/en
- Ouvrez un terminal de commande et tapez : 
- - `node --version` --> Doit afficher la version de node (Si non --> Mauvaise installation)

# Installer Docker

- 
-
- 

# Installer Angular 

- Ouvrez un terminal de commande 
- Tapez la commande : 
- - `npm install -g @angular/cli`

## Si installation manuel

- Ouvrez un terminal de commande
- Déplacez vous dans le dossier 'StreamX' du project
- Tapez la commande : `npm install`
- Tapez la commande : `ng serve`
- Allez sur l'URL : http://localhost:4200/

