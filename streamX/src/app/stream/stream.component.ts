import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

declare let Twitch: any;

@Component({
  selector: 'app-stream',
  templateUrl: './stream.component.html',
  styleUrl: './stream.component.css'
})
export class StreamComponent implements OnInit {
  @Input() streamerName:string
  streams: any

  private apiUrl = 'https://api.twitch.tv/helix/streams?language=fr&first=5';
  private clientId = 'pkx2favy4yo97cuimjqle2j8df3nif';
  private accessToken = 'ap5ygdl7rcenhag0kwu3nbc2tn4965';

  public constructor (private http: HttpClient, private activatedRoute: ActivatedRoute) {}



  ngOnInit() {
    //liste des streamers
    let headers = new HttpHeaders()
      .set("Client-ID", this.clientId)
      .set("Authorization", "Bearer " + this.accessToken);

    this.http.get(this.apiUrl, {headers: headers}).subscribe((response) => {
     this.streams = (<any>response).data 
    });

    this.streamerName = this.activatedRoute.snapshot.params['streamerName'];

    //intégration player + chat
    var options = {
      width: 1000,
      height: 500,
      channel: this.streamerName,
    };
    
    var player = new Twitch.Embed("twitch-embed", options);
    player.setVolume(0);
  }

}
